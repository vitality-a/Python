# -*- coding: utf-8 -*-
from tkinter import *
import os
import re
from tkinter.filedialog import askdirectory
import tkinter.messagebox
from tkinter import ttk


#资源管理器选择目录
def selectPath():
	Aimpath = askdirectory()
	path.set(str(Aimpath))


#获取该目录下所有文件，按格式修改后重命名
def fileType():
	tempPath = path.get()
	try:
		fileList=os.listdir(tempPath)
	except Exception as e:
		tkinter.messagebox.showerror('错误', '无效路径')
		return
	n=0
	for i in fileList:
	    #设置旧文件名(就是路径+文件名)
	    oldname=tempPath + "/" + fileList[n]

	    sco=re.search(r'[12]\d{8}', fileList[n])
	    if sco is None:
	    	sco="(学号未匹配)"
	    else:
	    	sco=sco.group()


	    name=re.search(r'[\u4e00-\u9fa5]{2,4}', fileList[n])
	    if name is None:
	    	name="(姓名未匹配)"
	    else:
	    	name=name.group()


	    ftype=re.search(r'\.(\w+)$',fileList[n])
	    if ftype is None:
	    	tkinter.messagebox.showerror('错误', '出错了')
	    	return
	    else:
	    	ftype=ftype.group()


	    example=""
	    if midPart.get() == "姓名学号":
	    	example = fristPart.get()+name+sco+endPart.get()
	    elif midPart.get() == "学号姓名":
	    	example = fristPart.get()+sco+name+endPart.get()
	    elif midPart.get() == "学号+姓名":
	    	example = fristPart.get()+sco+"+"+name+endPart.get()
	    elif midPart.get() == "姓名+学号":
	    	example = fristPart.get()+name+"+"+sco+endPart.get()
	    elif midPart.get() == "学号-姓名":
	    	example = fristPart.get()+sco+"-"+name+endPart.get()
	    elif midPart.get() == "姓名-学号":
	    	example = fristPart.get()+name+"-"+sco+endPart.get()
	    else:
	    	example = fileList[n]
	    #设置新文件名
	    newname=tempPath + "/" +example+ftype
	    os.rename(oldname,newname)   #用os模块中的rename方法对文件改名
	    n+=1
	tkinter.messagebox.showinfo('提示', '修改成功')


# 更改中间字段的格式
def transform(event):
	midPart.set(midbox.get())



#tk初始化
tk=Tk()
tk.title("学号姓名批量重命名工具")
tk.resizable(width=False, height=False)
tk.geometry("600x300+350+250")
tk.iconbitmap(r"C:\Users\lgl\Desktop\logo.ico")

#组件变量
path = Variable()
fristPart = Variable()
endPart = Variable()
midPart = Variable()
midPart.set("学号姓名") #默认顺序

#组件
Label(tk, text="注意:使用前请务必保证文件名只存在学号和姓名两项").place(x = 100, y=20)
Label(tk, text="文件路径:").place(x = 15, y=60)
Entry(tk, textvariable = path, width=50, bd=2).place(x=80,y=60)
Button(tk, text = "路径选择", command = selectPath).place(x=460,y=55)
Label(tk, text="前缀:").place(x = 8, y=110)
Entry(tk, textvariable = fristPart, bd = 2,width=15).place(x=45, y=110)
Label(tk, text="格式：").place(x = 180, y=110)


midbox = ttk.Combobox(tk, state="readonly")
midbox['value']=("学号姓名","姓名学号", "学号+姓名", "姓名+学号", "姓名-学号","学号-姓名")
midbox.current(0)
midbox.bind("<<ComboboxSelected>>", transform)
midbox.place(x=220, y=110)


Label(tk, text="后缀：").place(x = 400, y=110)
Entry(tk, textvariable = endPart, bd = 2,width=15).place(x=450, y=110)
Label(tk, text="重命名示例：(Web程序设计)190970237李XX(第一次作业)").place(x=130, y=140)
Button(tk, text = "修改", command = fileType,width=10).place(x=260, y=170)
Label(tk, text="帮助:使用前请先确认目标文件的所有文件名里9位数字只有学号，汉字只有姓名，可以存在符号。\n \
	首先选择需要修改文件名的路径，再选择需要的命名格式（如：学号姓名）\n \
	再按需求选择是否添加前缀或后缀，最后点击修改").place(x = 30, y=220)


tk.mainloop()